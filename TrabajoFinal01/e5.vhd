----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:33:59 07/11/2022 
-- Design Name: 
-- Module Name:    e5 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Ejercicio_05_FLIP is
    Port ( D,RESET,clk : in  STD_LOGIC;
           Q : out  STD_LOGIC);
end Ejercicio_05_FLIP;

architecture Behavioral of Ejercicio_05_FLIP is

begin
	process (clk,D,RESET)
	begin
		if RESET='1' then
			Q <= '0';
		elsif(clk' event and clk='1') then
			Q <= D;
		end if;
	end process;
end Behavioral;
