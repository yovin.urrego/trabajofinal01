----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ejercicio_02_latch is
    Port ( dato, control : in  STD_LOGIC;
           salida : out  STD_LOGIC);
end Ejercicio_02_latch;

architecture Behavioral of Ejercicio_02_latch is

begin
	process (dato,control)
	begin
		if control='1' then
			salida <=dato;
		end if;
	end process;
end Behavioral;

