----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:39:13 07/11/2022 
-- Design Name: 
-- Module Name:    e9 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ejercicio_09_registro is
	Port ( d, clk : in  STD_LOGIC;
           q : out  STD_LOGIC);
end Ejercicio_09_registro;

architecture Behavioral of Ejercicio_09_registro is

begin
	process (clk)
	variable a: STD_LOGIC;
	variable b: STD_LOGIC;
	begin
		if(clk' event and clk='1') then 
			a := d;
			b := a;
			q <= b;
		end if;
	end process;

end Behavioral;
