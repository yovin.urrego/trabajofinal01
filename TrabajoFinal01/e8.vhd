----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:37:27 07/11/2022 
-- Design Name: 
-- Module Name:    e8 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Ejercicio_08_registro is
    Port ( d, clk : in  STD_LOGIC;
           q : out  STD_LOGIC);
end Ejercicio_08_registro;

architecture serie of Ejercicio_08_registro is
signal a,b: STD_LOGIC;
begin
	process (clk)
	begin
		if(clk' event and clk='1') then 
			a <= d;
			b <= a;
			q <= d;
		end if;
	end process;
end serie;

