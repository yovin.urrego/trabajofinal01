
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Ejercicio_01_latch is
    Port ( x, control : in  STD_LOGIC;
           z : out  STD_LOGIC);
end Ejercicio_01_latch;

architecture Behavioral of Ejercicio_01_latch is

begin
	z <= x when (control= '1');
end Behavioral;
