----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:31:48 07/11/2022 
-- Design Name: 
-- Module Name:    e4 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Ejercicio_04_ffd is
Port ( D,CLK : in  STD_LOGIC;
           Q : out  STD_LOGIC);
end Ejercicio_04_ffd;

architecture forma01 of Ejercicio_04_ffd is

begin
	process (CLK )
	begin
		if (CLK' event and CLK='1') then
			Q <=D;
		end if;
	end process;
end  forma01;

architecture forma02 of Ejercicio_04_ffd is

begin
	process (CLK )
	begin
		if (rising_edge(CLK)) then
			Q <=D;
		end if;
	end process;
end forma02;

